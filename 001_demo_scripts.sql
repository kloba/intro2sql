/*
	1. Вступ до PgAdmin
  - показати вміст декількох таблиць, вибираючи із меню:
	"SELECT LIMIT 1000"
*/

/*
	2. Перший запит
*/

-- НАЗВА_СХЕМИ.НАЗВА_ТАБЛИЦІ
-- доступи, організація об'єктів
SELECT *
FROM Production.Product

SELECT *
FROM Person.Person

-- якщо не вказати назви схеми
SELECT *
FROM Person


-- вибираємо колонки
SELECT ProductID
  , Name
  , Color
  , Size
FROM Production.Product
-- ProductID = (PRIMARY KEY, PK)

-- Псевдоніми
SELECT ProductID AS ID
  , Name AS ProductName
  , Color AS ProductColor
  , Size AS ProductSize
FROM Production.Product


/*
	3. Підказки у PgAdmin - Intellisense
	-- практика написання запитів
*/

SELECT *
FROM Production.product



/*
	4. Сортування даних
*/

-- Сортування даних по замовчуванню
SELECT *
FROM Production.Product


-- рядки посортовані ЗРОСТАЮЧЕ згідно значень у колонці Name
-- = продукти посортовані по назві
SELECT *
FROM Production.Product
ORDER BY Name

-- порядок сортування СПАДАЮЧЕ
SELECT *
FROM Production.Product
ORDER BY Name DESC

-- два рівня сортування
-- продукти, відсортовані за кольором, а ті, що мають однаковий колір - за назвою
SELECT Color, Name
FROM Production.Product
ORDER BY Color, Name

-- спаданням по кольорах та за зростанням по іменах
SELECT *
FROM Production.Product
ORDER BY Color DESC, Name

/*
	5. Фільтрування даних
*/

SELECT ProductID
  , Name
  , Color
  , Size
FROM Production.Product
WHERE ProductID = 707

SELECT ProductID
  , Name
  , Color
  , Size
FROM Production.Product
WHERE ProductID > 100

SELECT ProductID
  , Name
  , Color
  , Size
FROM Production.Product
WHERE ProductID BETWEEN 13 AND 290

-- текстові значення - в одинарних лапках
SELECT ProductID
  , Name
  , Color
  , Size
FROM Production.Product
WHERE Color = 'Red'

SELECT ProductID
  , Name
  , Color
  , Size
FROM Production.Product
WHERE Size = 'M'

-- Оператор LIKE
SELECT *
FROM Production.Product
WHERE Name LIKE 'B%'

-- % - будь-який символ у довільній кількості
SELECT *
FROM Production.Product
WHERE Name LIKE '%Bike%'

-- _ будь-який один символ
SELECT *
FROM Production.Product
WHERE Name LIKE 'Mountain Bike Socks, _'

--! = замість LIKE
SELECT *
FROM Production.Product
WHERE Name = '%Bike%'





-- Оператори AND OR

-- вироби чорного кольору, розмір M
SELECT ProductID
  , Name
  , Color
  , Size
FROM Production.Product
WHERE Color = 'Black'
  AND Size = 'M'

-- чорні, срібні та сині вироби
SELECT ProductID
  , Name
  , Color
  , Size
FROM Production.Product
WHERE Color = 'Black'
  OR Color = 'Silver'
  OR Color = 'Blue'

-- поєднуємо оператори
SELECT *
FROM Production.Product
WHERE Name LIKE '%Bike%'
  AND Color = 'White'


-- NULL

-- невизначені значення
SELECT *
FROM Production.Product
ORDER BY Color

--! чи дійсно нема таких рядків?
SELECT *
FROM Production.Product
WHERE Color = NULL

SELECT *
FROM Production.Product
WHERE Color IS NULL

SELECT *
FROM Production.Product
WHERE Color IS NOT NULL


-- тепер все разом
SELECT *
FROM Production.Product
WHERE Color = 'Black'
  AND Size IS NOT NULL
  AND Name LIKE '%Frame%'


-- дужки
-- вироби із чорного кольору та розмір M АБО червоні дорожні товари
SELECT *
FROM Production.Product
WHERE (Color = 'Black' AND Size = 'M')
	OR (Color = 'Red' AND Name LIKE '%Road%')

--! увага до взаємовиключних умов
SELECT *
FROM Production.Product
WHERE Color = 'Black'
	AND Color = 'Red'

-- але...
SELECT *
FROM Production.Product
WHERE Color = 'Black'
  OR Color = 'Red'


-- або...

SELECT *
FROM Production.Product
WHERE Color IN ('Black', 'Red')


/*
	6. LIMIT
  Обмеження результатів
*/

SELECT *
FROM production.product
ORDER BY listprice DESC
LIMIT 10


/*
	References:
    - kursysql.pl
    - sql-ex.ru
*/
