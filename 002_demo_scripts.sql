/*
	7. Функції
*/

-- Скалярні функції:

SELECT NOW();

SELECT NOW() AS CurrentDateTime;


SELECT UPPER('Текст написаний маленькими буквами');
-- https://www.postgresql.org/docs/12/functions-string.html

-- https://www.postgresql.org/docs/12/functions-datetime.html
SELECT DATE_PART('year', timestamp '2020-09-22');

SELECT DATE_PART('year', NOW());

-- Назва продуктів великими літерами
SELECT ProductID
  , UPPER(Name) AS ProductName
  , Color AS ProductColor
  , Size AS ProductColor
FROM Production.Product;

-- Скільки днів пройшло від продажу продукту
SELECT ProductID
  , Name
  , Color AS Kolor
  , Size
  , DATE_PART('day', NOW() - SellStartDate)
FROM Production.Product;


-- Функції агрегаційні
SELECT COUNT(*) AS FnCount 
FROM Production.Product;

SELECT SUM(ListPrice) AS FnSum 
FROM Production.Product;

SELECT MIN(ListPrice) AS FnMIN 
FROM Production.Product
-- WHERE Color = 'Red';


/*
	8. Групування даних
*/

-- кількість продуктів кожного із кольорів
SELECT Color
  , COUNT(*) AS Cnt
FROM Production.Product
GROUP BY Color;

--! ми не можемо відображати стовпці, за якими ми не згруповані
SELECT Color
  , Size
  , COUNT(*) AS Cnt
FROM Production.Product
GROUP BY Color;


-- потрібно додати групування по розміру
SELECT Color
  , Size
  , COUNT(*) AS Cnt
FROM Production.Product
GROUP BY Color, Size;


/*
	9. Поєднання таблиць
*/


SELECT * 
FROM Production.Product;

SELECT * 
FROM Production.ProductSubcategory;

SELECT *
FROM Production.Product
JOIN Production.ProductSubcategory
  ON Production.Product.ProductSubcategoryID = Production.ProductSubcategory.ProductSubcategoryID;

-- alias
SELECT *
FROM Production.Product AS p
JOIN Production.ProductSubcategory AS ps
  ON p.ProductSubcategoryID = ps.ProductSubcategoryID;

--! Дві таблиці із колонкою Name
SELECT ProductID
  , Name
  , Color
  , Size
  , Name
FROM Production.Product AS p
JOIN Production.ProductSubcategory AS ps
  ON p.ProductSubcategoryID = ps.ProductSubcategoryID;


SELECT p.ProductID
  , p.Name
  , p.Color
  , p.Size
  , ps.Name as SubCategoryName
FROM Production.Product AS p
JOIN Production.ProductSubcategory AS ps
  ON p.ProductSubcategoryID = ps.ProductSubcategoryID;


/*
	References: www.kursysql.pl
	            www.sql-ex.ru
*/